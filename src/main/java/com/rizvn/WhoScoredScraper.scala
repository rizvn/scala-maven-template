package com.rizvn

import java.net.URL
import java.nio.charset.StandardCharsets

import org.jsoup.Jsoup

/**
  * @author Riz
  */
class WhoScoredScraper {

  def scrapeMatchPage(url: String): Unit = {
    val pageUrl = new URL(url)
    val doc = Jsoup.parse(pageUrl, 30000)
    val statDivs = doc.select("#chalkboard-stadium li.filterz-option")
    doc.charset(StandardCharsets.UTF_8)
    print(statDivs)
  }
}
